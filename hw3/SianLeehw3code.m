clear;
clc;
addpath('C:\Users\Sian\Documents\econ_512_2017\CEtools')
%  Do not use absolute path, it is different on each computer
%% Empiricla Method
% HW 3
% Sian Lee(consulted with Hyung-gyu Rho)
%% PART A c,d
Y = 1.1:0.01:3;
the1 = 5;
the1A = 5;
iter = 5000;
theta1 = zeros(length(Y),1);
%Use Newton's method without calling any general optimization or
%rootfinding solvers.
for l = 1:length(Y)
    for i = 1:iter
        the1 = the1A-(log(the1A)-log(Y(l))-psi(the1A))/(1/the1A - trigamma(the1A));
        if abs(the1-the1A)<eps
            break;
        else
            the1A = the1;
        end
    end
    theta1(l) = the1;
end

%Plot theta1 as a function of Y1/Y2
hold on
plot(Y,theta1);
xlabel('theta1'); ylabel('Y1/Y2');
hold off

%% Part B
clear;
clc;
%load data
load hw3.mat;
Y=y;
n=size(X,1);
k=size(X,2);

%% Question 1
%(a) FMINUNC without a derivative supplied
betaini = zeros(k,1);
opta = optimoptions('fminunc','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');
tic
[b_1a,fval_1a,flag_1a,out_1a,grad_1a,hess_1a] = fminunc(@(b) logL(b,X,Y),betaini,opta);
toc
disp('estimated parameters 1(a): ');
disp(b_1a');
disp(['number of iterations: ', num2str(out_1a.iterations)]);
disp(['number of function evaluations: ', num2str(out_1a.funcCount)]);

%(b) FMINUNC with a derivative supplied
optb = optimoptions('fminunc','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs');
tic
[b_1b,fval_1b,flag_1b,out_1b,grad_1b,hess_1b] = fminunc(@(b) logL(b,X,Y),betaini,optb);
toc
disp('estimated parameters 1(b): ');
disp(b_1b');
disp(['number of iterations: ', num2str(out_1b.iterations)]);
disp(['number of function evaluations: ', num2str(out_1b.funcCount)]);

%(c) Nelder Mead
betainic = [1;0;0;0;0;-0.5];
tic
[b_1c,fval_1c,flag_1c,out_1c] = fminsearch(@(b) logL(b,X,Y),betainic);
toc
disp('estimated parameters 1(c): ');
disp(b_1c');
disp(['number of iterations: ', num2str(out_1c.iterations)]);
disp(['number of function evaluations: ', num2str(out_1c.funcCount)]);

%(d) BHHH
toler = .0000001;diff = 10;b_1d = betaini;iter = 0;
tic
while diff >= toler
    hess_app = 0;
    for i=1:n
        lnQd = -exp(X(i,:)*b_1d)*X(i,:)' + Y(i)*X(i,:)';
        hess_app = lnQd * lnQd' + hess_app;
    end
    d = inv( hess_app )* logLd(b_1d,X,Y)';
    b_1d_new = b_1d + d;
    diff = max( abs(b_1d_new - b_1d) );
    b_1d = b_1d_new;
    iter = iter + 1;
end
toc
disp('estimated parameters 1(d): ');
disp(b_1d');
disp(['number of iteration: ', num2str(iter)]);

%% Question 2
hesini = hessian_approx(betaini,X,Y);
hess = hessian_approx(b_1d,X,Y);
disp('The eigenvalues for the initial Hessian: ');
disp(eig(hesini));
disp('The eigenvalues for the Hessian at the estimated parameters: ');
disp(eig(hess));

%% Question 3
tic
[b_3,resnorm,residual,flag_3,out_3] = lsqnonlin(@(b) Y-exp(X*b),betaini);
toc
disp('estimated parameters for NLLS: ');
disp(b_3');
disp(['number of iterations: ', num2str(out_3.iterations)]);
disp(['number of function evaluations: ', num2str(out_3.funcCount)]);

%% Question 4
BHHHstderr=diag(sqrt(inv(-hess)./length(Y)))
G = zeros(6,6);
H = 0;
for i = 1:length(Y)
    G = G + (X(i,:)'.*exp(X(i,:)*b_3)^2*X(i,:));
    H = H + (Y(i) - exp(X(i,:)*b_3))^2.*(X(i,:)'.*exp(X(i,:)*b_3)^2*X(i,:));
end
H = 4/length(Y).*H;
G = 1/length(Y).*G;
psi = G\H/G;
NLLSstderr = diag(sqrt(inv(psi)./length(Y)))