function [ L_i ] = like_1(beta,gam,X,Y,Z)
L_i = prod((((1+exp(-beta*X-gam*Z)).^(-1)).^Y) .* ((1-(1+exp(-beta*X-gam*Z)).^(-1)).^(1-Y)) );
end
