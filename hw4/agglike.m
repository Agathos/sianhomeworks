function [ L, L_i ] = agglike( beta_0, gam, u_0, sig_beta, sig_beta_u, sig_u, data, P )
L_init = zeros(data.N,1);
mu = beta_0;
sigma = sig_beta;
[XX, WW] = qnwnorm(P, mu, sigma);
MM = length(WW);

for i=1:data.N
    x = data.X(:,i); 
    y = data.Y(:,i); 
    z = data.Z(:,i); 
    L_int_temp=nan(MM,1); 
    for j=1:MM
       L_int_temp(j)=like_1(XX(j,1),gam,x,y,z);
    end
        L_init(i) = WW'*L_int_temp;
end

L_i = L_init;
L = -sum(log(L_i)); 
end

