function [ L_i ] = like_2( beta,gam,u,X,Y,Z )
L_i = prod(((1+exp(-beta*X-gam*Z-u)).^(-1)).^Y.*(1-(1+exp(-beta*X-gam*Z-u)).^(-1)).^(1-Y));
end