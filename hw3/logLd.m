function [result] = logLd(b,X,Y)
k = size(X,2);
A = kron(exp(X*b),ones(1,k)); 
B = kron(Y,ones(1,k)); 
C = -A.*X + B.*X;
result = sum(C); 
end