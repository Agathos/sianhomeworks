function [ logL, logLd ] = logL( b,X,Y )
A = -exp(X*b) + Y.*(X*b) - log(factorial(Y));
logL = -sum(A);
k = size(X,2);
B = kron(exp(X*b),ones(1,k)); 
C = kron(Y,ones(1,k)); 
D = -B.*X + C.*X;
logLd = -sum(D);
end