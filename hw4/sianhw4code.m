%% HW4 Sian Lee

clear;
clc;
load('hw4data.mat');
addpath('C:\Users\Sian\Documents\econ_512_2017\CompEcon\CEtools');
% first question asekd to calculate the likelihood at some value. but I did
% not reduce points for that. 

%% Case 1. Gaussian Quadrature using 20 nodes

nod20=20;
logagglike = @(B)agglike(B(1),B(2),B(3),B(4),B(5),B(6),data,nod20);
B_1 = [0.1; 0; 0; 1; 0; 0];
[bhat_1, fval_1] = fminunc(logagglike, B_1);

disp(' <Case 1> ');
disp('1. Starting value'); 
disp(['beta_0: ',num2str(B_1(1))]);
disp(['gamma: ',num2str(B_1(2))]);
disp(['u_0: ',num2str(B_1(3))]);
disp(['sigma_beta: ',num2str(B_1(4))]);
disp(['sigma_beta&u: ',num2str(B_1(5))]);
disp(['sigma_u: ',num2str(B_1(6))]);

disp('2. Argmax');
disp(['beta_0: ',num2str(bhat_1(1))]);
disp(['gamma: ',num2str(bhat_1(2))]);
disp(['u_0: ',num2str(bhat_1(3))]);
disp(['sigma_beta: ',num2str(bhat_1(4))]);
disp(['sigma_beta&u: ',num2str(bhat_1(5))]);
disp(['sigma_u: ',num2str(bhat_1(6))]);

disp('3. Maximized value of the Likelihood function');
disp([': ',num2str(-fval_1)]);




%% Case 2. Monte Carlo Methods using 100 nodes
nod100=100;
logagglikemonte = @(B)agglike_monte(B(1),B(2),B(4),data,nod100);
B_2 = [0.1; 0; 0; 1; 0; 0];
[bhat_2, fval_2] = fminunc(logagglikemonte, B_2);

disp(' <Case 2> ');
disp('1. Starting value'); 
disp(['beta_0: ',num2str(B_2(1))]);
disp(['gamma: ',num2str(B_2(2))]);
disp(['u_0: ',num2str(B_2(3))]);
disp(['sigma_beta: ',num2str(B_2(4))]);
disp(['sigma_beta&u: ',num2str(B_2(5))]);
disp(['sigma_u: ',num2str(B_2(6))]);

disp('2. Argmax');
disp(['beta_0: ',num2str(bhat_2(1))]);
disp(['gamma: ',num2str(bhat_2(2))]);
disp(['u_0: ',num2str(bhat_2(3))]);
disp(['sigma_beta: ',num2str(bhat_2(4))]);
disp(['sigma_beta&u: ',num2str(bhat_2(5))]);
disp(['sigma_u: ',num2str(bhat_2(6))]);

disp('3. Maximized value of the Likelihood function');
disp([': ',num2str(-fval_2)]);

%% Case 3. u_0 is not equal to zero. Monte Carlo using 100 nodes
nod100=100;
logagglikemonte3 =  @(B)agglike_monte2(B(1),B(2),B(3),B(4),B(5),B(6),data,nod100);
B_3=[0.1; 0; 0.3; 1; 0.3; 0.3];
[bhat_3, fval_3] = fminunc(logagglikemonte3, B_3);

disp(' <Case 3> ');
disp(['beta_0: ',num2str(B_3(1))]);
disp(['gamma: ',num2str(B_3(2))]);
disp(['u_0: ',num2str(B_3(3))]);
disp(['sigma_beta: ',num2str(B_3(4))]);
disp(['sigma_beta&u: ',num2str(B_3(5))]);
disp(['sigma_u: ',num2str(B_3(6))]);

disp('2. Argmax');
disp(['beta_0: ',num2str(bhat_3(1))]);
disp(['gamma: ',num2str(bhat_3(2))]);
disp(['u_0: ',num2str(bhat_3(3))]);
disp(['sigma_beta: ',num2str(bhat_3(4))]);
disp(['sigma_beta&u: ',num2str(bhat_3(5))]);
disp(['sigma_u: ',num2str(bhat_3(6))]);

disp('3. Maximized value of the Likelihood function');
disp([': ',num2str(-fval_3)]);


