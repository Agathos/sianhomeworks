function [ L, L_i ] = agglike_monte2( beta_0, gam, u_0, sig_beta, sig_beta_u, sig_u, data, P )
mu = [beta_0, u_0];
sigma = [sig_beta, sig_beta_u; sig_beta_u, sig_u];
rng default
r = mvnrnd(mu,sigma,P);
beta = r(:,1);
u = r(:,2);
WW = (1/P) * ones(P,1);

for i=1:data.N
    x = data.X(:,i); 
    y = data.Y(:,i); 
    z = data.Z(:,i); 
    L_int_temp=nan(P,1); 
    for j=1:P
       L_int_temp(j)=like_2(beta(j,1),gam,u(j,1),x,y,z);
    end
   
    L_init(i) = WW'*L_int_temp;
end

L_i = L_init;

L = -sum(log(L_i)); 

end
