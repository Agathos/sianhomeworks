%% For this code, (especailly for the Q 3 ~ 5) I mainly consulted with Hyung Gyu Rho.
clc
clear
addpath('C:\Users\Sian\Documents\econ_512_2017\CEtools')
%% Question 1
V = [-1 -1 -1];
P = [1 1 1];
q_a = exp(V(1)-P(1))/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
q_b = exp(V(2)-P(2))/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
q_c = exp(V(3)-P(3))/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
q_0 = 1/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
Q=[q_a q_b q_c q_0]
sum(Q)

%% Question 2
Initial_P=[1,0,0,3;1,0,1,2;1,0,2,1];
tic
for i=1:4
    options=optimset('Display','iter','Display','final','Maxfunevals',1000,'TolX',sqrt(eps)); 
    [root(:,i),FVAL2(:,i),flag(i),OUTPUT2(i)]=fsolve('foc',Initial_P(:,i),options);
end
toc 
for i=1:4
    if flag(i)==1
    disp(['Initial Guess:' num2str(Initial_P(:,i)')])
    disp(['Root:' num2str(root(:,i)')]);
    
    else
    disp(['Initial Guess:' num2str(Initial_P(:,i)')])
    disp('Not conv.:')
    end
end
% the last iterations does not to converge right, I think it's because of
% the singularity in the FOC
%% Question 3
V = [-1 -1 -1];
P = [1 1 1];
tic
j=1;
tol=0.1^10;
Pn=P;
Po=P+3;
while (norm(Po-Pn)>tol)
    P=Pn;
    for i=1:500
        q_a=exp(V(1)-P(1))/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
        P(1) = 1 / (1-q_a);
        c1 = q_a-exp(V(1)-P(1))/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
        if abs(c1)<tol
            break;
        end
    end
    for i=1:500
         q_b=exp(V(2)-P(2))/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
        P(2) = 1 / (1-q_b);
        c2 = q_b-exp(V(2)-P(2))/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
        if abs(c2)<tol
            break;
        end
    end
        for i=1:500
         q_c=exp(V(3)-P(3))/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
        P(3) = 1 / (1-q_c);
        c3 = q_c-exp(V(3)-P(3))/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
        if abs(c3)<tol
            break;
        end
    end
    Po=Pn;
    Pn=P;
    j=j+1;
    if j>=5000
        break;
    end
end
toc

%%Question 4
V = [-1 -1 -1];
P = [1 1 1];
j=1;
tol=0.1^10;
Pn4=P;
Po4=P+3;
while (norm(Po4-Pn4)>tol)
    P=Pn4;
    for i = 1:500
        Q = exp(V-P)./(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
        P = P./(Q-1);
        crit = norm(Q-exp(V-P)./(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3))));
        if crit<tol
            break;
        end
    end
    Po4=Pn4;
    Pn4=P;
    j=j+1;
    if j>=5000
        break;
    end
end
toc

%% Question 5.

V = [-1 -1 -1];
P = [3 2 1];
q_a = exp(V(1)-P(1))/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
q_b = exp(V(2)-P(2))/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
q_c = exp(V(3)-P(3))/(1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
P(1)=1/(1-q_a);
P(2)=1/(1-q_b);
P(3)=1/(1-q_c);
Ps

% you were meant to run this until convergence, but that's fine. 

% what is PS? 