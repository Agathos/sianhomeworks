function [result] = hessian_approx(b,X,Y)
hess_app = 0;
n = size(X,1);
for i=1:n
    d_lnQ = -exp(X(i,:)*b)*X(i,:)' + Y(i)*X(i,:)';
    hess_app = d_lnQ * d_lnQ' + hess_app;
end
result = hess_app;
end