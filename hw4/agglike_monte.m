function [ L, L_i ] = agglike_monte( beta_0, gam, sig_beta, data, P )
rng default
beta_2 = sig_beta * randn(P,1) + beta_0;
WW = (1/P) * ones(P,1);
for i=1:data.N
    x = data.X(:,i); 
    y = data.Y(:,i);
    z = data.Z(:,i);
    L_int_temp=nan(P,1);
    for j=1:P
       L_int_temp(j)=like_1(beta_2(j,1),gam,x,y,z);
    end
      L_init(i) = WW'*L_int_temp;
end

L_i = L_init;

L = -sum(log(L_i));

end
