diary
%%Question 1
X=[1 1.5 3 4 5 7 9 10];
Y1=-2+0.5*X;
Y2=-2+0.5*X.^2;
plot(X,Y1)
hold on
plot(X,Y2)
hold off
grid
title 'Y1 and Y2 wrt X'
xlabel('X'), ylabel('Y1 and Y2')

%%Question 2
Z=linspace(-10,20,200)'
sZ=sum(Z)

%%Question 3
A=[2 4 6;1 7 5; 3 12 4];
B=[-2;3;10];
C=A'*B

% inv is slow, use the \ operator as in the answer key
D=inv(A'*A)*B
E=sum(A'*B)
F=[A(1,1:2);A(3,1:2)]

% the same, inv is slow.
x=inv(A)*B

%%Question 4
K=kron(eye(5),A)

%%Question 5
A5=random('Normal',10,5,5,3)

% it is easier and faster to use A = A<10
for i=1:5;
    for j=1:3;
        if A5(i,j)<10,A5(i,j)=0;
        else A5(i,j)=1;
        end
    end
end

%%Question 6
load('datahw1.csv')
fitlm([datahw1(:,3:4) datahw1(:,6)],datahw1(:,5))

diary