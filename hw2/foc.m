function [fval]=foc(P)
    V=[-1,-1,-1];
    q_a=exp(V(1)-P(1)) / (1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
    q_b=exp(V(2)-P(2)) / (1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
    q_c=exp(V(3)-P(3)) / (1+exp(V(1)-P(1))+exp(V(2)-P(2))+exp(V(3)-P(3)));
    foc_a=q_a*(1-P(1)+q_a*P(1));
    foc_b=q_b*(1-P(2)+q_b*P(2));
    foc_c=q_c*(1-P(3)+q_c*P(3));
    fval=[foc_a; foc_b; foc_c];    
end